using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Undestroyble : MonoBehaviour
{
    private void Awake()
    {

        int numberOfMusic = FindObjectsOfType<Undestroyble>().Length;
        if (numberOfMusic > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
