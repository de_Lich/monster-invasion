using UnityEngine;
using Enemy;

public class CollisionControl : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemyAI>().Die();
        }
    }
}
