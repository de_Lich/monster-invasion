using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Enemy;

namespace General
{
    public class TimeController : MonoBehaviour
    {
        public static TimeController instance;
        GameManager gameManager;
        [SerializeField] Text timer;
        [SerializeField] Text countdownText;

        private float startTime;
        private float elapsedTime;
        [SerializeField] float finishTime;
        public int countdownTime;

        TimeSpan timePlaying;
        private bool gamePlaying = false;

        private void Awake()
        {
            instance = this;
        }
        void Start()
        {
            gameManager = GameManager.singleton;
            gamePlaying = false;
            Time.timeScale = 1;
            StartCoroutine(CountdownToStart());
        }
        private void BeginGame()
        {
            startTime = Time.time;
            gamePlaying = true;
        }
        private void Update()
        {
            if (gamePlaying)
            {
                elapsedTime = Time.time - startTime;
                timePlaying = TimeSpan.FromSeconds(elapsedTime);

                string timePlayingStr = "Time: " + timePlaying.ToString("mm':'ss");
                timer.text = timePlayingStr;
            }
            CheckTime();
        }

        private void CheckTime()
        {
            if(elapsedTime >= finishTime)
            {
                //GameManager gameManager = GetComponent<GameManager>();
                gameManager.WinCondition();
            }
        }

        private IEnumerator CountdownToStart()
        {
            while (countdownTime > 0)
            {
                countdownText.text = countdownTime.ToString();
                yield return new WaitForSeconds(1f);
                countdownTime--;
            }
            BeginGame();
            countdownText.text = "GO!";
            yield return new WaitForSeconds(1f);
            countdownText.gameObject.SetActive(false);
        }
        //public void FreezeGame()
        //{
        //    StartCoroutine(FreezeCoroutine());
        //}
        //private IEnumerator FreezeCoroutine()
        //{
        //    var gameObjects = FindObjectsOfType<EnemyAI>();
        //    for (int i = 0; i < gameObjects.Length; i++)
        //    {
        //        var rb = gameObjects[i].transform.GetComponent<EnemyAI>();
        //        rb.isAlive = false;
        //    }
        //    yield return new WaitForSeconds(5f);
        //    for (int i = 0; i < gameObjects.Length; i++)
        //    {
        //        var rb = gameObjects[i].transform.GetComponent<EnemyAI>();
        //        rb.isAlive = true;
        //    }
        //}
    }
}
