using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enemy;

namespace General
{
    public class ClickController : MonoBehaviour
    {
        AudioManager audioManager;
        private new Camera camera;
        private bool isGameRunning;
        [SerializeField] GameObject monsterbBlood;
        [SerializeField] int damage = 1;

        void Start()
        {
            camera = GetComponent<Camera>();
            isGameRunning = true;
            GameManager.StoppingGame += StopClicking;
            audioManager = AudioManager.singleton;
        }

        void Update()
        {
            if (isGameRunning)
            {
                Clicking();
            }
        }

        private void Clicking()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    EnemyAI enemy = hit.collider.gameObject.GetComponent<EnemyAI>();
                    if (enemy)
                    {
                        GameObject impact = Instantiate(monsterbBlood, hit.point, Quaternion.identity);
                        Destroy(impact, 0.5f);
                        enemy.TakeDamage(damage);
                        audioManager.Play("Shot");
                    }
                    else
                    {
                        audioManager.Play("MissClick");
                    }
                }
            }
        }
        private void StopClicking()
        {
            isGameRunning = false;
        }
        private void OnDestroy()
        {
            GameManager.StoppingGame -= StopClicking;
        }
    }
}
