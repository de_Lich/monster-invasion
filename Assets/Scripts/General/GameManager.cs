using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Enemy;
using System;

namespace General
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager singleton { get; private set; }
        AudioManager audioManager;
        [SerializeField] Text enemyScoretxt;
        [SerializeField] Text enemyCaptxt;
        [SerializeField] GameObject looseMenu;
        [SerializeField] GameObject winMenu;
        [SerializeField] int enemyCap = 0;
        [SerializeField] int enemyCount = 0;

        public static event Action StoppingGame; 
        private void Awake()
        {
            singleton = this;
        }
        void Start()
        {
            audioManager = AudioManager.singleton;
            FindObjectOfType<MusicManager>().GetComponent<AudioSource>().Play();
            winMenu.SetActive(false);
            looseMenu.SetActive(false);
            EnemySpawn.Added += CountingEnemy;
            EnemyAI.Removed += Score;
            EnemyAI.Removed += DisCountingEnemy;
        }
        private void Lose()
        {
            FindObjectOfType<MusicManager>().GetComponent<AudioSource>().Stop();
            audioManager.Play("LooseSound");
            Time.timeScale = 0;
            looseMenu.SetActive(true);
            StoppingGame();

        }
        public void WinCondition()
        {
            FindObjectOfType<MusicManager>().GetComponent<AudioSource>().Stop();
            audioManager.Play("Win");
            Time.timeScale = 0;
            winMenu.SetActive(true);
            StoppingGame();
        }
        public void Score()
        {
            enemyCount++;
            enemyScoretxt.text = "Score: " + enemyCount.ToString();
            if (PlayerPrefs.GetInt("Score") <= enemyCount)
            {
                PlayerPrefs.SetInt("Score", enemyCount);
            }
        }
        public void CountingEnemy()
        {
            enemyCap++;
            enemyCaptxt.text = "Enemies: " + enemyCap.ToString() + "/10";
            if(enemyCap >= 10)
            {
                Lose();
            }
        }
        public void DisCountingEnemy()
        {
            enemyCap--;
            if(enemyCaptxt != null)
            {
                enemyCaptxt.text = "Enemies: " + enemyCap.ToString() + "/10";
            }
        }
        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
        public void ToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }
        private void OnDestroy()
        {
            EnemySpawn.Added -= CountingEnemy;
            EnemyAI.Removed -= Score;
            EnemyAI.Removed -= DisCountingEnemy;
        }
    }
}
