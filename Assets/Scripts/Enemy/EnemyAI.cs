using General;
using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemy
{

    public class EnemyAI : MonoBehaviour
    {
        Animator animator;
        EnemySpawn enemySpawn;
        AudioManager audioManager;
        CapsuleCollider collider;
        public HealthBar healthBar;
        [SerializeField] Light spotLight;

        [SerializeField]private int maxHitPoints;
        private int difficultyInterval;
        private int currentHitPoints;
        public float speed = 5f;
        private float obstacleRange = 2f;
        public bool isAlive;
        public bool isReady;
        public bool isDead;

        public float gameTimeCount;

        public static event Action Removed;
        public static event Action Upgraded;
        private void Awake()
        {
            gameTimeCount = Time.timeSinceLevelLoad;
            ChangeConditions();
        }
        private void Start()
        {
            enemySpawn = EnemySpawn.singleton;
            difficultyInterval = enemySpawn.difficultyInterval;
            audioManager = AudioManager.singleton;
            healthBar.SetMaxHealth(maxHitPoints);
            collider = GetComponent<CapsuleCollider>();
            animator = GetComponent<Animator>();
            spotLight.enabled = true;
            isReady = true;
            isDead = false;
            StartCoroutine(SpawnPosition());
        }
        private void Update()
        {
            if (isAlive)
            {
                MovingPermanently();
            }
        }
        private void MovingPermanently()
        {
            animator.SetBool("isRunning", true);
            transform.Translate(0, 0, speed * Time.deltaTime);

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.SphereCast(ray, 2, out hit))
            {
                if (hit.distance < obstacleRange)
                {
                    StartCoroutine(IdlePosition());
                }
            }
        }

        public void TakeDamage(int damage)
        {
            currentHitPoints -= damage;
            healthBar.SetHealth(currentHitPoints);
            if (currentHitPoints <= 0)
            {
                Die();
            }
        }

        public void Die()
        {
            Destroy(collider);
            collider.enabled = false;
            isAlive = false;
            animator.SetBool("isDead", true);
            Removed();
            audioManager.Play("MonsterDeath");
            spotLight.enabled = false;
            isDead = true;
            Destroy(gameObject, 2.5f);
        }

        private void RotateProcess()
        {
            float angle = Random.Range(-130, 130);

            transform.Rotate(0, angle, 0);
        }

        IEnumerator IdlePosition()
        {
            animator.SetBool("isRunning", false);
            isAlive = false;
            yield return new WaitForSeconds(.5f);
            if (!isDead)
            {
                RotateProcess();
                isAlive = true;
            }
            else if (isDead)
            {
                isAlive = false;
                animator.Play("fallingback");
            }
        }

        IEnumerator SpawnPosition()
        {
            collider.enabled = false;
            while (isReady)
            {
                isAlive = false;
                yield return new WaitForSeconds(2f);
                animator.SetBool("isReady", true);
                collider.enabled = true;
                isReady = false;
                isAlive = true;
            }
        }

        public void ChangeConditions()
        {
            if (gameTimeCount >= 18)
            {
                speed++;
                maxHitPoints++;
            }
            if (gameTimeCount >= 33)
            {
                speed++;
                maxHitPoints++;
            }
            currentHitPoints = maxHitPoints;
        }
    }
}
