using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using General;
using UnityEngine.UI;

namespace Enemy
{
    public class EnemySpawn : MonoBehaviour
    {
        public static EnemySpawn singleton;
        AudioManager audioManager;
        [SerializeField] EnemyAI normalEnemyPrefab;
        [SerializeField] EnemyAI bigEnemyPrefab;
        [SerializeField] EnemyAI smallEnemyPrefab;
        [SerializeField] Text warningtxt;

        [SerializeField] GameObject spawnPlate;


        public float interval = 3;                 // TO DO random interval
                                                   
        [SerializeField]private float xLength, zLength;
        public int difficultyInterval;
        private float timeCount;

        float xDistance, zDistance;

        public static event Action Added;
        void Awake()
        {
            timeCount = Time.timeSinceLevelLoad;
            singleton = this;
        }
        void Start()
        {
            audioManager = AudioManager.singleton;
            GameManager.StoppingGame += StopAllCoroutines;
            warningtxt.gameObject.SetActive(false);
            StartCoroutine(MonsterSpawnCycle());
            InvokeRepeating("ChangeConditions", difficultyInterval + 3, difficultyInterval);
        }
        private void Update()
        {

        }
        private IEnumerator MonsterSpawnCycle()
        {
            yield return new WaitForSeconds(TimeController.instance.countdownTime);
            while (true)
            {
                int randomEnemy = Random.Range(1, 4);
                if (randomEnemy == 1)
                {
                    MonsterAppear(smallEnemyPrefab);
                }
                else if (randomEnemy == 2)
                {
                    MonsterAppear(normalEnemyPrefab);
                }
                else if (randomEnemy == 3)
                {
                    MonsterAppear(bigEnemyPrefab);
                }
                yield return new WaitForSeconds(interval);
            }
        }

        private void MonsterAppear(EnemyAI enemyPrefab)
        {
            if (enemyPrefab != null)
            {
                Vector3 randomPos = Vector3.zero;

                xDistance = Random.Range(-xLength, xLength);
                zDistance = Random.Range(-zLength, zLength);

                randomPos = new Vector3(xDistance, 0, zDistance);
                Instantiate(enemyPrefab, randomPos, Quaternion.Euler(0, Random.Range(0, 360), 0));
                //if (timeCount >= 18.0f)
                //{
                //    enemyPrefab.ChangeConditions();
                //}
                //if (timeCount >= 33.0f)
                //{
                //    enemyPrefab.ChangeConditions();
                //    enemyPrefab.ChangeConditions();
                //}
                Added();
            }
        }
        private void ChangeConditions()
        {
            StartCoroutine(Warning());
            interval -= 0.5f;
        }
        private IEnumerator Warning()
        {
            int count = 0;
            while (count <= 2)
            {
                warningtxt.gameObject.SetActive(true);
                audioManager.Play("Warning");
                yield return new WaitForSeconds(.5f);
                warningtxt.gameObject.SetActive(false);
                yield return new WaitForSeconds(.5f);
                count++;
            }
        }
        private void OnDestroy()
        {
            GameManager.StoppingGame -= StopAllCoroutines;
        }
    }
}
